package com.raymank26.dominoes.test

import com.raymank26.dominoes.model.{Element, Graph}
import com.raymank26.dominoes.{DominoesGenerator, Solver}

import org.scalatest.{FunSuite, Matchers}

/**
 * @author Anton Ermak.
 */
class TestSuite extends FunSuite with Matchers {

    test("Full set should contain 28 elements") {
        DominoesGenerator.FullSet should have size 28
    }

    test("Graph building should works on all input values") {
        1.to(28).foreach { n =>
            DominoesGenerator.generateGraph(n)
        }
    }

    test("Simple graph should have one component") {
        checkComponents(Seq(Element(1, 1)), isOne = true)
    }

    test("Graph should have one component") {
        val elements = Seq(
            Element(0, 1),
            Element(1, 2),
            Element(1, 3),
            Element(3, 3)
        )
        checkComponents(elements, isOne = true)
    }

    test("Graph should have more than one component") {
        val elements = Seq(
            Element(0, 1),
            Element(4, 5),
            Element(1, 2),
            Element(1, 3),
            Element(3, 3)
        )
        checkComponents(elements, isOne = false)
    }

    test("Check solver properties") {
        val elements = Seq(
            Element(1, 1),
            Element(1, 2),
            Element(2, 3),
            Element(3, 1)
        )
        Solver(Graph(elements)).hasEulerianLoop shouldBe true
    }

    private def checkComponents(elements: Seq[Element], isOne: Boolean): Unit = {
        Solver(Graph(elements)).hasOneComponent shouldBe isOne
    }

}
