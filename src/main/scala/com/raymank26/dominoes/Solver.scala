package com.raymank26.dominoes

import com.raymank26.dominoes.model.Graph

import scala.collection.mutable.ArrayBuffer

/**
 * @author Anton Ermak.
 */
class Solver private(graph: Graph) {

    val hasOneComponent = checkForComponents()

    /**
     * The graph has Eulerian path when the number of vertexes with odd orders is less or equal of 2.
     */
    def hasEulerianChain: Boolean = {
        hasOneComponent && graph.orders.count(_ % 2 == 1) <= 2
    }

    /**
     * The graph has Eulerian loop when the vertexes' orders is even.
     */
    def hasEulerianLoop: Boolean = {
        hasOneComponent && !graph.orders.exists(_ % 2 != 0)
    }

    /**
     * Checks that graph contains only one component by  breadth first search algorithm.
     *
     * @return `true` if [[Graph]] contains one connected component and `false` otherwise.
     */
    private def checkForComponents(): Boolean = {
        val queue = scala.collection.mutable.Queue[Int]()
        val visits = ArrayBuffer.fill[Boolean](graph.vertexes.max + 1)(false)
        queue += graph.vertexes.head

        while (queue.nonEmpty) {
            val from = queue.dequeue()
            val neighbours = graph.neighbours(from)
            for {
                vertex <- neighbours if !visits(vertex)
            } {
                visits(vertex) = true
                queue += vertex
            }
        }
        graph.vertexes.forall(v => visits(v))
    }
}

object Solver {

    def apply(graph: Graph) = new Solver(graph)

}
