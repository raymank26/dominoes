package com.raymank26.dominoes.model

/**
 * @author Anton Ermak.
 */
case class Element(left: Int, right: Int) {

    override def toString: String = {
        s"Left = $left, Right = $right"
    }
}
