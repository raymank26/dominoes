package com.raymank26.dominoes.model

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

/**
 * Undirected graph.
 *
 * @author Anton Ermak.
 */
class Graph private(adjacencyList: Seq[Set[Int]], val vertexes: Set[Int]) {

    def orders: Seq[Int] = adjacencyList.zipWithIndex.map { case (e, i) =>
        e.count(_ != i)
    }

    def neighbours(vertex: Int): Iterable[Int] = {
        adjacencyList(vertex)
    }

    override def toString = s"Graph($vertexes, $orders)"
}

object Graph {

    private val MaxDice = 6

    def apply(elements: Seq[Element]) = {
        val array = ArrayBuffer.fill[mutable.HashSet[Int]](MaxDice + 1)(mutable.HashSet())

        val vertexes = collection.mutable.HashSet[Int]()

        elements.foreach { element =>
            array(element.left) += element.right
            array(element.right) += element.left

            vertexes += element.left
            vertexes += element.right
        }
        new Graph(array.map(_.toSet), vertexes.toSet)
    }

}
