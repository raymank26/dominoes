package com.raymank26.dominoes

import com.raymank26.dominoes.model.{Element, Graph}

/**
 * @author Anton Ermak.
 */
object Main {

    def main(args: Array[String]) {
        val size = getSetSize(args)
        size match {
            case Left(msg) => println(msg)
            case Right(setSize) => solve(setSize)
        }
    }

    private def solve(setSize: Int) = {
        val dominoes = DominoesGenerator.generateSet(setSize)
        printDominoes(dominoes)
        val solver = Solver(Graph(dominoes))

        println( s"""Answers:
                    |1) -
                    |2) Can be arranged to loop - ${printBoolean(solver.hasEulerianLoop) }
                    |3) Can be arranged to chain - ${printBoolean(solver.hasEulerianChain) }
                    |""".stripMargin)
    }

    private def getSetSize(args: Array[String]): Either[String, Int] = {
        try {
            Right(Integer.parseInt(args(0)))
        } catch {
            case e: NumberFormatException =>
                Left("The first argument should be number")
            case e: IndexOutOfBoundsException =>
                Left("The integer as first argument should be provided")
        }
    }

    private def printDominoes(elements: Seq[Element]): Unit = {
        println("The generated dominoes is:")
        elements.foreach(println)
    }

    private def printBoolean(boolean: Boolean): String = {
        boolean match {
            case true => "Yes"
            case false => "No"
        }
    }

}
