package com.raymank26.dominoes

import com.raymank26.dominoes.model.{Element, Graph}

import scala.collection.immutable.IndexedSeq
import scala.util.Random

/**
 * Generates dominoes or graph.
 *
 * @author Anton Ermak.
 */
object DominoesGenerator {

    val FullSet: IndexedSeq[Element] = for {
            left <- 0 to 6
            right <- left to 6
        } yield Element(left, right)

    def generateSet(n: Int): IndexedSeq[Element] = {
        require(n > 0 && n <= 28)
        Random.shuffle(FullSet).take(n)
    }

    def generateGraph(n: Int): Graph = {
        Graph(generateSet(n))
    }

}
